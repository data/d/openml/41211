# OpenML dataset: ames-housing

https://www.openml.org/d/41211

**WARNING: This dataset is still in preparation.**

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

A processed version of the 'Ames Iowa Housing' dataset as provided by the make_ames() function in the R-package 'AmesHousing' [Max Kuhn (2017). AmesHousing: The Ames Iowa Housing Data. R package version 0.0.3. https://CRAN.R-project.org/package=AmesHousing]. The original data was published in [De Cock, D. (2011). 'Ames, Iowa: Alternative to the Boston Housing Data as an End of Semester Regression Project', Journal of Statistics Education, Volume 19, Number 3]. For a description of the dataset checkout either the documentation of the R-package or the original publication. The variable 'Sale_Price' was chosen as target variable. Note that all factors are unordered in this version of the dataset as provided by the make_ames() function, in contrast to the version provided by the make_ordinal_ames() function.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/41211) of an [OpenML dataset](https://www.openml.org/d/41211). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/41211/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/41211/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/41211/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

